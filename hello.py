from numpy import random

import math
#------------------------------CUSTOM LIST--------------------------------------#
# arr = []
# n = int(input("Insert number of elements: "))
# for i in range(0,n):
#    elem = int(input())
#    lst.append(elem)
# print(arr)

#-----------------------------RANDOM LIST-------------------------------------#
n = int(input("Number of indexes: "))
arr = random.randint(1,21,size=(n))  # randomly generate from 0 to 10 with n indexes
print(arr)
#-----------------------------SANITIZING INPUT-----------------------#
def get_non_negative_int(prompt):
    while True:
        try:
            value = int(input(prompt))
        except ValueError:
            print("Sorry please input the correct way")
            continue

        if value < 0:
            print("Only posi vibes man")
            continue
        else:
            break
    return value
#-----------------------------1-------------------------------------#
def sum_list():
    print("Sum of elements in list is: ",sum(arr)) # sum(list) will sum all indexes in list
    elem1 = get_non_negative_int("Choose the first index: ")
    elem2 = get_non_negative_int("Choose the second index: ") + 1  # +1 because in list slice, the second parameter will take the one before it
    if elem1 < 0 or elem2 < 0:
        print("please input a positive number")
    else:
        rs = sum(arr[elem1:elem2])  # sum all indexes in sliced list   
        print("Sum from %s to %s = %s" % (elem1,elem2-1,rs))


#-----------------------------2-------------------------------------#
def multiply_list():
    rs = 1
    rs2 = 1
    for i in arr:
        rs = rs * i
    print("Multiply all indexes in list we got:", rs)
    elem1 = get_non_negative_int("Choose the first index: ")
    elem2 = get_non_negative_int("Choose the second index: ") + 1

    for i in arr[elem1:elem2]:
        rs2 = rs2 * i
    print("Multiply from %s to %s = %s" % (elem1,elem2-1,rs2))

#-----------------------------3-------------------------------------#
def find_prime():
    prime = []
    for i in arr:
        c = 0;
        for j in range(1,i):    # get position from 1 to i in arr
            if i % j == 0:  
                c+=1
        if c==1:
            prime.append(i)  
    print("Prime numbers in list are: " ,prime)


#-----------------------------4-------------------------------------#
def print_prime():
    n = get_non_negative_int("Input your number: ")
    for num in range(2,n+1):    # iterate from 2 to number + 1 because range take < 
        if all(num % i != 0 for i in range(2,num)):
            print(num)


#-----------------------------5-------------------------------------#
def print_fibbo():
    n = get_non_negative_int("How many number you want in your Fibbo sequence: ")

    n1, n2 = 0, 1   # first 2 number of fibbo
    count = 0;

    if n <=0:
        print("Please insert a positive number")
    elif n == 1:
        print("Fibbo sequence upto %s: " % n)
        print("n1")
    else:
        print("Fibbo sequence: ")
        while count < n:
            print(n1)
            sum = n1 + n2
            # update value of n1, n2 and sum
            n1 = n2
            n2 = sum
            count += 1

#-----------------------------6-------------------------------------#
def is_fibbo():
    n = get_non_negative_int("Insert a number to see if it in fibbo sequence: ")
    n1, n2 = 0, 1   # first 2 number of fibbo
    count = 0;

    if n == 0 or n == 1:
        print("Yes, %s is a Fibbo number" % n)
    else:
        while count < n:
            count = n1 + n2
            n1 = n2
            n2 = count
        if(count == n): # if count meets n while doing the calculating
            print("Yes, %s is a Fibbo number" % n)
        else:
            print("No, %s is not a Fibbo number" % n)

#-----------------------------7-------------------------------------#
def sort(array):
    less = []
    equal = []
    greater = []

    if len(array) > 1:
        pivot = array[0]
        for x in array:
            if x < pivot:
                less.append(x)
            elif x == pivot:
                equal.append(x)
            elif x > pivot:
                greater.append(x)
        return sort(less) + equal + sort(greater)   # with recursive way, continue sorting less[] and greater[] until length in array = 1
    else:
        return array

def binary_search(arr, item):
    begin_index = 0
    end_index = len(arr) - 1

    while begin_index <= end_index:
        midpoint = begin_index + (end_index - begin_index) // 2
        midpoint_value = arr[midpoint]
        if midpoint_value == item:
            return midpoint

        elif item < midpoint_value:
            end_index = midpoint - 1

        else:
            begin_index = midpoint + 1

    return None

#-----------------------------8-------------------------------------#
sum_list()
multiply_list()
find_prime()
print_prime()
print_fibbo()
is_fibbo()
print("Sorted list: ", sort(arr))
item = int(input("Insert item you want to find in list: "))
print("The item you want to find is in the position: %sth" % binary_search(sort(arr),item,))
